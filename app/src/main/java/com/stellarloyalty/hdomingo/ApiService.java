package com.stellarloyalty.hdomingo;

import com.stellarloyalty.hdomingo.model.Cards;
import com.stellarloyalty.hdomingo.model.Deck;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * ***************************************************************************
 * Copyright (c) 2016 Stellar Loyalty, Inc. All rights reserved.              *
 * ****************************************************************************
 */
public interface ApiService {
    @GET("new/shuffle/?deck_count=1")
    Call<Deck> newDeck();

    @GET("{id}/draw/?count=13")
    Call<Cards> showCards(@Path("id") String deckId);
}
