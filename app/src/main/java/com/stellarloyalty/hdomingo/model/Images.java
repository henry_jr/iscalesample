package com.stellarloyalty.hdomingo.model;

/**
 * ***************************************************************************
 * Copyright (c) 2016 Stellar Loyalty, Inc. All rights reserved.              *
 * ****************************************************************************
 */
public class Images {
    public String svg;
    public String png;
}
