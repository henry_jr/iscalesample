package com.stellarloyalty.hdomingo.model;

/**
 * ***************************************************************************
 * Copyright (c) 2016 Stellar Loyalty, Inc. All rights reserved.              *
 * ****************************************************************************
 */
public class CardDetails {
    public String image;
    public String code;
    public String value;
    public String suit;
    public Images images;
}
