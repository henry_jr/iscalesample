package com.stellarloyalty.hdomingo.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stellarloyalty.hdomingo.R;
import com.stellarloyalty.hdomingo.model.ItemFragment.OnListFragmentInteractionListener;
import com.stellarloyalty.hdomingo.model.dummy.DummyContent.DummyItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final List<CardDetails> mValues;
    private final Context mContext;

    public MyItemRecyclerViewAdapter(List<CardDetails> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mSuit.setText(mValues.get(position).suit);
        holder.mValue.setText(mValues.get(position).value);
        Picasso.with(mContext).load(mValues.get(position).image).into(holder.mCard);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mSuit;
        public final TextView mValue;
        public final ImageView mCard;
        public CardDetails mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mSuit = (TextView) view.findViewById(R.id.txtSuit);
            mValue = (TextView) view.findViewById(R.id.txtValue);
            mCard = (ImageView) view.findViewById(R.id.imgCard);
        }

    }
}
