package com.stellarloyalty.hdomingo.model;

import java.util.List;

/**
 * ***************************************************************************
 * Copyright (c) 2016 Stellar Loyalty, Inc. All rights reserved.              *
 * ****************************************************************************
 */
public class Cards {
    public int remaining;
    public String deck_id;
    public List<CardDetails> cards;
    public boolean success;
}
