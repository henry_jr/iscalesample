package com.stellarloyalty.hdomingo.model;

/**
 * ***************************************************************************
 * Copyright (c) 2016 Stellar Loyalty, Inc. All rights reserved.              *
 * ****************************************************************************
 */
public class Deck {

    public int remaining;
    public boolean shuffled;
    public String deck_id;
    public boolean success;
}
