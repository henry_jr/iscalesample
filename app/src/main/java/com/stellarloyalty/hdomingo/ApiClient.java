package com.stellarloyalty.hdomingo;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ***************************************************************************
 * Copyright (c) 2016 Stellar Loyalty, Inc. All rights reserved.              *
 * ****************************************************************************
 */
public class ApiClient {
    private static final String URL = "http://deckofcardsapi.com/api/deck/";
    private static Retrofit mRetrofit;

    public static Retrofit getClient(){
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
            return mRetrofit;
    }
}
