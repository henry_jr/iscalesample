package com.stellarloyalty.hdomingo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.stellarloyalty.hdomingo.model.CardDetails;
import com.stellarloyalty.hdomingo.model.Cards;
import com.stellarloyalty.hdomingo.model.Deck;
import com.stellarloyalty.hdomingo.model.MyItemRecyclerViewAdapter;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {
    Button btnDealNow;
    Retrofit client;
    ApiService service;
    Button btnFirst;
    Button btnSecond;
    Button btnThird;
    Button btnFourth;
    RecyclerView recyclerView;
    String deckId;
    MyItemRecyclerViewAdapter adapter;
    HashMap<Integer, List<CardDetails>> cache;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        client = ApiClient.getClient();
        service =  client.create(ApiService.class);

        btnDealNow = (Button) findViewById(R.id.btnDeal);
        btnFirst = (Button) findViewById(R.id.btnFirst);
        btnSecond = (Button) findViewById(R.id.btnSecond);
        btnThird = (Button) findViewById(R.id.btnThird);
        btnFourth = (Button) findViewById(R.id.btnFourth);
        recyclerView = (RecyclerView) findViewById(R.id.listCards);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        btnDealNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<Deck> decks = service.newDeck();
                decks.enqueue(new Callback<Deck>() {
                    @Override
                    public void onResponse(Call<Deck> call, Response<Deck> response) {
                        deckId = response.body().deck_id;
                        cache = new HashMap<>();

                        Toast.makeText(MainActivity.this, "You can now show players' cards.", Toast.LENGTH_LONG).show();

                        btnFirst.setVisibility(View.VISIBLE);
                        btnSecond.setVisibility(View.VISIBLE);
                        btnThird.setVisibility(View.VISIBLE);
                        btnFourth.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFailure(Call<Deck> call, Throwable t) {
                        //TODO
                    }
                });
            }
        });
    }

    public void showCards(View v){
        //TODO show cards of the four players iwth images
        int player = 0;
        switch (v.getId()){
            case R.id.btnFirst:
                player = 1;
                break;
            case R.id.btnSecond:
                player = 2;
                break;
            case R.id.btnThird:
                player = 3;
                break;
            case R.id.btnFourth:
                player = 4;
                break;
        }
        getPlayerCard(deckId, player);
    }

    private void getPlayerCard(String deckId, final int player){
        Call<Cards> cards = service.showCards(deckId);
        if (cache.containsKey(player)){
            adapter = new MyItemRecyclerViewAdapter(cache.get(player), MainActivity.this);
            recyclerView.setAdapter(adapter);
            return;
        }
        cards.enqueue(new Callback<Cards>() {
            @Override
            public void onResponse(Call<Cards> call, Response<Cards> response) {
                cache.put(player, response.body().cards);
                adapter = new MyItemRecyclerViewAdapter(response.body().cards, MainActivity.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Cards> call, Throwable t) {
                //TODO
                Toast.makeText(MainActivity.this, "Reshuffle cards", Toast.LENGTH_LONG).show();
            }
        });
    }
}
